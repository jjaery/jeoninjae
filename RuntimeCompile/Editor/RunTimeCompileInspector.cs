﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.CodeDom;
using System.CodeDom.Compiler;
using Microsoft.CSharp;
using System.Reflection;
using System.Text;


public class RuntimeCompileWindow : EditorWindow
{

    private static bool foldout1 = false, foldout2 = false, foldout3 = false, foldout4 = false, listVisibility = true, listVisibility2 = true;
    private static RunTimeCompile script;

    [MenuItem("Dev/RuntimeCompiler %#r")]
    public static void Init()
    {
        RuntimeCompileWindow window = (RuntimeCompileWindow)GetWindow(typeof(RuntimeCompileWindow));
        if (script == null)
        {
            script = new RunTimeCompile();
        }

        foldout1 = GetBool("RuntimeCompile.foldout1");
        foldout2 = GetBool("RuntimeCompile.foldout2");
        foldout3 = GetBool("RuntimeCompile.foldout3");
        foldout4 = GetBool("RuntimeCompile.foldout4");
        listVisibility = GetBool("RuntimeCompile.listVisibility");
        listVisibility2 = GetBool("RuntimeCompile.listVisibility2");
        script.Init();
        window.Show();
    }


    private static bool GetBool(string key)
    {
        if (EditorPrefs.HasKey(key))
            return EditorPrefs.GetBool(key);
        else
            return false;
    }

    void OnGUI()
    {
        DrawProperties();
    }


    private static void SaveCSFile(string str, string defFileName)
    {
        if (string.IsNullOrEmpty(str))
        {
            EditorUtility.DisplayDialog("빈 파일 경고", "저장하려는 문자열이 빈 공백입니다.", "확인");
            return;
        }

        var path = EditorUtility.SaveFilePanel(
                "Save it",
                "",
                defFileName,
                "cs");
        if (path != null)
        {
            System.IO.File.WriteAllText(path, str);
            if (EditorUtility.DisplayDialog("저장성공", "성공적으로 저장이 완료되었습니다.\r\n파일을 여시겠습니까?", "열기", "닫기"))
            {
                System.Diagnostics.Process.Start(path);
            }
        }
    }
    private static void SaveCSFile(string str)
    {
        if (string.IsNullOrEmpty(str))
        {
            EditorUtility.DisplayDialog("빈 파일 경고", "저장하려는 문자열이 빈 공백입니다.", "확인");
            return;
        }

        var path = GetTempFilePathWithExtension(".cs");
        if (path != null)
        {
            System.IO.File.WriteAllText(path, str);
            System.Diagnostics.Process.Start(path);
        }
    }
    private static string GetTempFilePathWithExtension(string extension)
    {
        var path = System.IO.Path.GetTempPath();
        var fileName = System.Guid.NewGuid().ToString() + extension;
        return System.IO.Path.Combine(path, fileName);
    }


    private static void DrawProperties()
    {
        if (script == null)
        {
            script = new RunTimeCompile();
            script.Init();
        }
        EditorGUI.BeginChangeCheck();
        foldout1 = EditorGUILayout.Foldout(foldout1, "풀-코드");
        if (foldout1)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("전체 코드를 입력하세요.");
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginScrollView(new Vector2(0, 0), GUILayout.Height(400));
            script._fullcode = EditorGUILayout.TextArea(script._fullcode, GUILayout.Height(400));
            EditorGUILayout.EndScrollView();

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("풀 - 코드 실행"))
            {
                script.Compile(true);
            }
            EditorGUILayout.EndHorizontal();
            if (GUILayout.Button(".cs 파일로 저장"))
            {
                SaveCSFile(script._fullcode, "_fullcode.cs");
            }
            if (GUILayout.Button(".cs 파일로 바로 열기"))
            {
                SaveCSFile(script._fullcode);
            }
        }

        EditorGUILayout.Space();

        foldout2 = EditorGUILayout.Foldout(foldout2, "코드");

        if (foldout2)
        {
            EditorGUI.indentLevel++;
            var list = script._usingList;
            listVisibility = EditorGUILayout.Foldout(listVisibility, string.Format("Using List - Current : {0} Usings", list.Count));
            if (listVisibility)
            {
                EditorGUI.indentLevel++;
                EditorGUILayout.BeginHorizontal();
                if (GUILayout.Button("+", GUILayout.Width(30)))
                {
                    list.Add("");
                }
                if (GUILayout.Button("-", GUILayout.Width(30)))
                {
                    if (list.Count != 0)
                        list.RemoveAt(list.Count - 1);
                }
                EditorGUILayout.EndHorizontal();
                for (int i = 0; i < list.Count; i++)
                {
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(i.ToString(), GUILayout.Width(50));
                    list[i] = EditorGUILayout.TextArea(list[i]);
                    EditorGUILayout.EndHorizontal();
                }
                EditorGUI.indentLevel--;
            }
            EditorGUI.indentLevel--;

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("실행할 메소드 코드를 입력하세요.");
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginScrollView(new Vector2(0, 0), GUILayout.Height(400));
            script._code = EditorGUILayout.TextArea(script._code, GUILayout.Height(400));
            EditorGUILayout.EndScrollView();

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("코드 실행"))
            {
                var cls = script.Compile(false);
                if (cls != null)
                {
                    System.Type program = cls.GetType("RunTimeCompileClass1");
                    MethodInfo main = program.GetMethod("Main");
                    main.Invoke(null, null);
                }
            }
            EditorGUILayout.EndHorizontal();
            if (GUILayout.Button(".cs 파일로 저장"))
            {
                SaveCSFile(script._code, "_code.cs");
            }
        }

        EditorGUILayout.Space();
        foldout3 = EditorGUILayout.Foldout(foldout3, "메소드 호출");

        if (foldout3)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("클래스 : ", GUILayout.Width(50));
            script._classname = EditorGUILayout.TextField(script._classname, GUILayout.Width(150));
            EditorGUILayout.LabelField("메소드 :", GUILayout.Width(50));
            script._methodname = EditorGUILayout.TextField(script._methodname);
            EditorGUILayout.EndHorizontal();


            EditorGUI.indentLevel++;
            listVisibility2 = EditorGUILayout.Foldout(listVisibility2, string.Format("Parameter List - Current : {0} Usings", script._parameterList.Count));
            if (listVisibility2)
            {
                EditorGUI.indentLevel++;
                EditorGUILayout.BeginHorizontal();
                if (GUILayout.Button("+", GUILayout.Width(30)))
                {
                    script._parameterList.Add("");
                }
                if (GUILayout.Button("-", GUILayout.Width(30)))
                {
                    if (script._parameterList.Count != 0)
                        script._parameterList.RemoveAt(script._parameterList.Count - 1);
                }
                EditorGUILayout.EndHorizontal();
                for (int i = 0; i < script._parameterList.Count; i++)
                {
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("파라미터 값 : ", GUILayout.Width(100));
                    script._parameterList[i] = EditorGUILayout.TextField(script._parameterList[i]);
                    EditorGUILayout.EndHorizontal();
                }
                EditorGUI.indentLevel--;
            }

            EditorGUI.indentLevel--;

            if (GUILayout.Button("코드 실행"))
            {
                script.InvokeMethod();
            }
        }

        EditorGUILayout.Space();
        foldout4 = EditorGUILayout.Foldout(foldout4, "실시간 파일 컴파일");

        if (foldout4)
        {
            if (GUILayout.Button("코드 열기"))
            {
                script.TempCSFileOpen();
            }
            if (GUILayout.Button("코드 컴파일"))
            {
                script.CompileTempCSFile();
            }
        }

        if (EditorGUI.EndChangeCheck())
        {
            EditorPrefs.SetBool("RuntimeCompile.foldout1", foldout1);
            EditorPrefs.SetBool("RuntimeCompile.foldout2", foldout2);
            EditorPrefs.SetBool("RuntimeCompile.foldout3", foldout3);
            EditorPrefs.SetBool("RuntimeCompile.foldout4", foldout4);
            EditorPrefs.SetBool("RuntimeCompile.listVisibility", listVisibility);
            EditorPrefs.SetBool("RuntimeCompile.listVisibility2", listVisibility2);
        }
    }
}
