﻿#if UNITY_EDITOR
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.CodeDom;
using System.Linq;
using System.CodeDom.Compiler;
using Microsoft.CSharp;
using System.Reflection;
using System.Text;

[ExecuteInEditMode]
public class RunTimeCompile {

    private const string ori_code = @"
public class RunTimeCompileClass1{
public static void Main() 
{ 
{0}
}
}";

    private const string ori_unity_template = @"using UnityEngine;
using System.Collections;
 
public class #SCRIPTNAME# : MonoBehaviour {
 
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}";


    public string _fullcode,_code;
    public string _methodname,_classname;
    public List<string> _usingList = new List<string>();
    public List<string> _parameterList = new List<string>();
    private static CSharpCodeProvider provider = new CSharpCodeProvider(new Dictionary<string, string>
        {
            { "CompilerVersion", "v3.5" }
        });
        
    private static string csPath = string.Format("{0}/{1}", new DirectoryInfo(Application.dataPath).FullName, "Scripts/RuntimeCompile/.script/temp.cs");


    private string GetString(string key)
    {
        if (EditorPrefs.HasKey(key))
            return EditorPrefs.GetString(key);
        else
            return null;
    }

    private void GetLists()
    {
        if (EditorPrefs.HasKey("RunTimeCompile._usingList"))
        {
            _usingList.Clear();
            var str = EditorPrefs.GetString("RunTimeCompile._usingList");
            foreach(var _using in str.Split('|'))
            {
                _usingList.Add(_using);
            }
        }
        else
        {
            if (_usingList.Count == 0)
            {
                _usingList.Add("System.Collections");
                _usingList.Add("System.Collections.Generic");
                _usingList.Add("UnityEngine");
                _usingList.Add("UnityEditor");
            }
        }

        try
        {
            if (EditorPrefs.HasKey("RunTimeCompile._parameterList"))
            {
                _parameterList.Clear();
                var str = EditorPrefs.GetString("RunTimeCompile._parameterList");
                foreach (var _parameter in str.Split('|'))
                {
                    _parameterList.Add(_parameter);
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }

    }

    public void Init()
    {
        _code = GetString("RunTimeCompile._code");
        _fullcode = GetString("RunTimeCompile._fullcode");
        _classname = GetString("RunTimeCompile._classname");
        _methodname = GetString("RunTimeCompile._methodname");
        GetLists();     
    }


    public Assembly Compile(bool isFullcode)
    {
        string code = isFullcode ? _fullcode : _code;


        if(isFullcode)
        {
            EditorPrefs.SetString("RunTimeCompile._fullcode", _fullcode);
        }
        else
        {
            EditorPrefs.SetString("RunTimeCompile._code", _code);

            StringBuilder str = new StringBuilder();
            for(int i =0; i< _usingList.Count;i++)
            {
                str.Append(_usingList[i]);
                if (i+1 != _usingList.Count)
                {
                    str.Append("|");
                }
            }
            EditorPrefs.SetString("RunTimeCompile._usingList", str.ToString());
        }

        if (string.IsNullOrEmpty(code))
            return null;


        CompilerParameters parameters = GetParameters();
        foreach (var assembly in System.AppDomain.CurrentDomain.GetAssemblies())
        {
            parameters.ReferencedAssemblies.Add(assembly.Location);
        }


        string resultCode = null;
        StringBuilder strb;

        if (!isFullcode)
        {
            strb = new StringBuilder();

            foreach (var _using in _usingList)
            {
                strb.AppendLine(string.Format("using {0};", _using));
            }

            strb.Append(ori_code.Replace("{0}", code));
            resultCode = strb.ToString();
        }
        else
        {
            resultCode = code;
        }
        CompilerResults results = provider.CompileAssemblyFromSource(parameters, resultCode);

        if (results.Errors.Count != 0)
        {
            strb = new StringBuilder();

            strb.AppendLine(string.Format("컴파일 에러 ! ({0} ERRORS)", results.Errors.Count));

            foreach (CompilerError error in results.Errors)
            {
                strb.AppendLine(error.ErrorText);
            }
            strb.AppendLine(string.Format("Code : {0}", resultCode));
            Debug.LogError(strb.ToString());
            return null;
        }

        object classRef = results.CompiledAssembly.CreateInstance("_actions");
        classRef = results.CompiledAssembly;
        try
        {
            if (classRef is CompilerErrorCollection)
            {
                strb = new StringBuilder();

                foreach (CompilerError error in (CompilerErrorCollection)classRef)
                {
                    strb.AppendLine(string.Format("{0}:{1} {2} {3}",
                                       error.Line, error.Column, error.ErrorNumber, error.ErrorText));
                }
                Debug.LogError(strb.ToString());
                return null;
            }
        }
        catch (System.Exception ex)
        {
            Debug.LogError(ex.Message);
            throw;
        }

        return (Assembly)classRef;
    }



    public void InvokeMethod()
    {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < _parameterList.Count; i++)
        {
            str.Append(_parameterList[i]);
            if (i + 1 != _parameterList.Count)
            {
                str.Append("|");
            }
        }


        if (str.Length != 0)
        {
            EditorPrefs.SetString("RunTimeCompile._parameterList", str.ToString());
        }
        EditorPrefs.SetString("RunTimeCompile._classname", _classname);
        EditorPrefs.SetString("RunTimeCompile._methodname", _methodname);

        object[] param = null;

        if(_parameterList.Count != 0)
        {
            param = _parameterList.Select((item) => (object)item).ToArray();
        }

        System.Type classType = Type.GetType(_classname);
        if(classType == null)
        {
            Debug.LogError("클래스를 찾을 수 없습니다.");
            return;
        }

        MethodInfo mi = classType.GetMethod(_methodname, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);

        if(mi == null)
        {
            Debug.LogError("메소드를 찾을 수 없습니다.");
            return;
        }

        object target = null;
        if(classType.IsSubclassOf(typeof(UnityEngine.Object)))
        {
            target = UnityEngine.Object.FindObjectOfType(classType);
            if(target == null)
            {
                Debug.LogError("유니티 오브젝트 타겟을 찾을 수 없습니다.");
                return;
            }
        }
        else
        {
            Assembly a = Assembly.GetAssembly(classType);
            target = a.CreateInstance(_classname);
        }

        if(target == null)
        {
            Debug.LogError("타겟을 찾을 수 없습니다.");
            return;
        }

        mi.Invoke(target, param);
    }

    public void TempCSFileOpen()
    {
        CheckCSFile();
        System.Diagnostics.Process.Start(csPath);
    }

    public void CompileTempCSFile()
    {
        CheckCSFile();
        CompilerParameters parameters = GetParameters();
        foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
        {
            parameters.ReferencedAssemblies.Add(assembly.Location);
        }

        CompilerResults results = provider.CompileAssemblyFromFile(parameters, csPath);

        StringBuilder strb;
        if (results.Errors.Count != 0)
        {
            strb = new StringBuilder();

            strb.AppendLine(string.Format("컴파일 에러 ! ({0} ERRORS)", results.Errors.Count));

            foreach (CompilerError error in results.Errors)
            {
                strb.AppendLine(error.ErrorText);
            }
            Debug.LogError(strb.ToString());
        }
        else
        {
            foreach(var _type in results.CompiledAssembly.GetTypes())
            {
                if(_type.IsSubclassOf(typeof(UnityEngine.Object)))
                {
                    if(Application.isPlaying)
                    {
                        GameObject parent = GameObject.Find("runtimeCompile_object");
                        UnityEngine.Object obj = UnityEngine.Object.FindObjectOfType(_type);

                        if(parent == null)
                        {
                            parent = new GameObject("runtimeCompile_object");
                        }
                        if (obj != null)
                        {
                            UnityEngine.Object.Destroy(obj);
                        }
                        parent.AddComponent(_type);                        
                    }
                    else
                    {
                        Debug.LogError("유니티 오브젝트 클래스는 게임 플레이 중일때만 인스턴스화가 가능합니다.");
                    }
                }
                else
                {
                    results.CompiledAssembly.CreateInstance(_type.ToString());
                }
            }
        }
    }

    private void CheckCSFile()
    {
        if (!File.Exists(csPath))
        {
            Debug.LogError("다음 경로에 CS파일이 없어 자동생성합니다.\r\n경로:" + csPath);
            using (StreamWriter sw = new StreamWriter(File.Open(csPath, FileMode.Create), Encoding.UTF8))
            {
                sw.WriteLine(ori_unity_template);
            }
        }
    }

    private CompilerParameters GetParameters()
    {
        return new CompilerParameters
        {
            GenerateExecutable = false,
            GenerateInMemory = true,
            WarningLevel = 3,
            CompilerOptions = "/optimize",
            TreatWarningsAsErrors = false
        };
    }
}
#endif